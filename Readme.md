# Generic Pairing

This project is about generating generic pairings that can be used as pre-shared knowledge by applications.

The project consists of three loosely coupled modules:
1. **Pairing Daemon** (doing most of the actual work)
2. **Phonebook** (front-end)
3. **Generic Pairing Library** (defining the IPC interface and some default settings)

Currently supported platforms are:
* Linux
* Android


## Building

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Dependencies

Libraries and development files you'll need:
* <b>Qt5</b>
* <b>Crypto++</b>

If you want to build for Android, also have a look at:
* [Getting started with Qt for Android] (http://doc.qt.io/qt-5/androidgs.html)
* [Building Crypto++ for Android] (https://cryptopp.com/wiki/Android_%28Command_Line%29)

### Configuration

Depending on where your <b>Crypto++</b> library and headers are installed, you might need to change the paths in *PairingDaemon.pro*

```
INCLUDEPATH +=   "/path/to/your/headers"
LIBS        += -L"/path/to/your/library"
```

Then run the Qt5 configuration tool for each of the three modules by navigating to the respective folder and entering `qmake` (it might have a slightly different name, like `qmake-qt5`)

```
cd /path/to/your/build/GenericPairingLibrary/ && qmake
cd /path/to/your/build/PairingDaemon/         && qmake
cd /path/to/your/build/Phonebook/             && qmake
```

### Compiling

Now you should have a Makefile for each module.

You have to compile the **Generic Pairing Library** first, as the other two modules depend on it.
Feel free to change some (or all) of the default values defined in the library before compiling

```
cd /path/to/your/build/GenericPairingLibrary/ && make
```

Depending on where you prefer to put your compiled library, you might need to adjust the corresponding entries for `LIBS` (and `ANDROID_EXTRA_LIBS` if you're building for Android) in *PairingDaemon.pro* and *Phonebook.pro* and run `qmake` again before proceeding.

Then compile the other two modules

```
cd /path/to/your/build/PairingDaemon/ && make
cd /path/to/your/build/Phonebook/     && make
```


## Running

All you need to do now is to run the **Pairing Daemon** and the **Phonebook** executables.

Don't forget to set `LD_LIBRARY_PATH` to the paths of your **Generic Pairing Library** and <b>Crypto++</b> library, if you chose non-default paths.

```
export LD_LIBRARY_PATH=/path/to/your/build/GenericPairingLibrary/:/path/to/your/cryptopp/lib
cd /path/to/your/build/PairingDaemon/ && ./pairingd &
cd /path/to/your/build/Phonebook/     && ./phonebook
```

If you used a development environment (e.g. **QtCreator**) you can of course also run the projects from there instead.

You should now be able to create pairings using the **Phonebook** front-end.
Pairings are Diffie-Hellman shared secrets based on the *2048-bit MODP Group with 256-bit Prime Order Subgroup* from [section 2.3 of RFC 5114](https://tools.ietf.org/html/rfc5114#section-2.3).
They are stored in the *DefaultPairingPath* defined in the **Generic Pairing Library**, which is set to your **Pairing Daemon**'s build folder if you didn't change it.


## Built With

* [Qt5](https://doc.qt.io/qt-5/)
* [Crypto++](https://www.cryptopp.com/)
* [QtZeroConf](https://github.com/jbagg/QtZeroConf)
